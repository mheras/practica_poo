class Cuenta:
    def __init__(self, nom, num, sal):
        self.titular = nom
        self.numero = num
        self.saldo = sal
        pass

    def ingreso(self, ingreso):
        self.saldo = self.saldo + ingreso

    def reintegro(self, reintegro):
        self.saldo = self.saldo - reintegro

    def transferenciaM(self, dst, cantidad):
        self.ingreso(cantidad)
        dst.reintegro(cantidad)

    def transferenciaF(org, dst, cantidad):
        dst.ingreso(cantidad)
        org.reintegro(cantidad)

    def __str__(self):
        return "#{num} con saldo: {sal}".format(num=self.numero, sal=self.saldo)


cc1 = Cuenta("Pepe", 1, 0)
cc2 = Cuenta("Ana", 2, 5000)
print(cc1, cc2)

cc1.ingreso(3000)
cc2.reintegro(1000)
print(cc1, cc2)
cc2.transferenciaM(cc1, 1500)
Cuenta.transferenciaF(cc1, cc2, 1500)
print(cc1, cc2)